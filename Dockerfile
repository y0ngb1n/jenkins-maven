FROM jenkinsci/blueocean:1.8.2

MAINTAINER YangBin <ybin1212@qq.com>

USER root

# 时区
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# 下载安装 Docker CLI
RUN curl -O https://get.docker.com/builds/Linux/x86_64/docker-latest.tgz \
    && tar zxvf docker-latest.tgz \
    && cp docker/docker /usr/local/bin/ \
    && rm -rf docker docker-latest.tgz

# 参考：https://blog.lab99.org/post/docker-2016-07-14-faq.html#ru-he-zai-docker-rong-qi-nei-shi-yong-docker-ming-ling-bi-ru-zai-jenkins-rong-qi-zhong
# 将 `jenkins` 用户的组 ID 改为宿主 `docker` 组的组ID，从而具有执行 `docker` 命令的权限。
# 组 ID 使用了 DOCKER_GID 参数来定义，以方便进一步定制。构建时可以通过 --build-arg 来改变 DOCKER_GID 的默认值，运行时也可以通过 --user jenkins:1234 来改变运行用户的身份。
ARG DOCKER_GID=999
USER jenkins:${DOCKER_GID}

ENV JAVA_OPTS=-Dorg.apache.commons.jelly.tags.fmt.timeZone=Asia/Shanghai

# 安装 Maven
ENV MAVEN_VERSION 3.3.9

RUN mkdir /var/tmp/maven \
	&& wget -P /var/tmp/maven https://mirrors.cnnic.cn/apache/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz \
	&& tar xzf /var/tmp/maven/apache-maven-$MAVEN_VERSION-bin.tar.gz -C /var/tmp/maven \
	&& rm -rf /var/tmp/maven/apache-maven-$MAVEN_VERSION-bin.tar.gz

# 设置 Maven 环境变量 
ENV MAVEN_HOME=/var/tmp/maven/apache-maven-$MAVEN_VERSION \
	PATH=$MAVEN_HOME/bin:$PATH

# 配置 Jenkins 插件
COPY plugins.txt /usr/share/jenkins/plugins.txt
RUN /usr/local/bin/plugins.sh /usr/share/jenkins/plugins.txt

EXPOSE 8080
