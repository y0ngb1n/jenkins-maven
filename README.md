# Jenkins & Maven

[![](https://gitlab.com/ybin1212/jenkins-maven/badges/master/pipeline.svg)]() [![](https://img.shields.io/badge/jenkins blueocean-1.8.2-blue.svg)](https://hub.docker.com/r/jenkinsci/blueocean/) [![](https://img.shields.io/badge/maven-3.3.9-orange.svg)](https://mirrors.cnnic.cn/apache/maven/)

## 命令行方式

### 1. 构建镜像

```
docker build -t jenkins-maven .
```

### 2. 首先应该创建一个数据容器，使我们的配置能持久保留

```
docker run --name jenkins-maven-data jenkins-maven echo "Jenkins & Maven Data Container"
```

### 3. 启动 Jenkins 容器

```
docker run -d --name jenkins -p 8080:8080 \
	--volumes-from jenkins-maven-data \
	-v /var/run/docker.sock:/var/run/docker.sock \
	jenkins-maven
```

### 4. 测试 Docker 套接字

> [如何在 Docker 容器内使用 docker 命令(比如在 Jenkins 容器中)？](https://blog.lab99.org/post/docker-2016-07-14-faq.html#ru-he-zai-docker-rong-qi-nei-shi-yong-docker-ming-ling-bi-ru-zai-jenkins-rong-qi-zhong)

#### 4.1 查看宿主机上 `docker` 用户组的 `GID`

```
grep 'docker' /etc/group
```

#### 4.2 修改容器中 `jenkins` 用户的 `GID`

```
docker run -d --name jenkins -p 8080:8080 \
	--volumes-from jenkins-maven-data \
	-v /var/run/docker.sock:/var/run/docker.sock \
	--user jenkins:$(grep 'docker' /etc/group | tr -cd "[0-9]") \
	registry.gitlab.com/ybin1212/jenkins-maven:newest \
```

#### 4.3 进入容器

```
docker exec -it jenkins /bin/bash
# 查看 GID
id
# 测试 docker
docker ps
```

## docker-compose 方式

```
...
```

# 参考

- [乞丐版自动化部署 jenkins 环境搭建](https://juejin.im/post/5ac0f7d651882555784e1b4a)